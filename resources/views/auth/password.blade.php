@extends("app")



@section('content')
<div class="row">
    <div class="col-xs-12 col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <div class="panel-body">


            <h1>Recuperación de contraseña</h1>
            <hr>
            {!! Form::open( ["url" => "password/forgotten", "method" => "POST"]) !!}
                      <div class="form-group">
                        <label for="email-forgotten-input">Correo electrónico</label>
                        {!! Form::email('email', null, [ 'class' => "form-control", 'placeholder' => "Ingrese su correo electrónico", 'id' => "email-forgotten-input"]) !!}
                       <p class="help-block">Se le enviará un correo electrónico con los pasos a seguir.</p>
                      </div>
                      <button type="submit" class="btn btn-primary">Enviar</button>
            {!! Form::close() !!}
          </div>
        </div>
    </div>

</div>


@endsection

