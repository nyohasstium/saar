@extends("app")
@section("content")


<div class="row">
    <div class="col-xs-12 col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <div class="panel-body">


            <h1>Restablecimiento de contraseña</h1>
            <hr>
            {!! Form::open( ["url" => "password/recover", "method" => "POST"]) !!}
            <input type="hidden" name="token" value="{{ $token }}">
                      <div class="form-group">
                        <label for="email-recover-input">Correo electrónico</label>
                        {!! Form::email('email', null, [ 'class' => "form-control", 'placeholder' => "Ingrese su correo electrónico", 'id' => "email-recover-input"]) !!}
                      </div>
                    <div class="form-group">
                      <label for="password-recover-input">Nueva contraseña</label>
                      {!! Form::password('password',  [ 'class' => "form-control", 'placeholder' => "Ingrese su nueva contraseña", 'id' => "password-recover-input"]) !!}
                    </div>
                                        <div class="form-group">
                                          <label for="password_confirmation-recover-input">Confirmación de nueva contraseña</label>
                                          {!! Form::password('password_confirmation',  [ 'class' => "form-control", 'placeholder' => "Confirme su nueva contraseña", 'id' => "password_confirmation-recover-input"]) !!}
                                        </div>
                      <button type="submit" class="btn btn-primary">Aceptar</button>
            {!! Form::close() !!}
          </div>
        </div>
    </div>

</div>

@endsection