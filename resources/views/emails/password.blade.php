<table width="100%" cellspacing="0" cellpadding="0" border="0" height="100%" style="background-color:#efefef;">
<tbody>
<tr>
<td valign="top" align="center">
<table width="640" cellspacing="0" cellpadding="0" border="0" style="border:0;background-color:#efefef;padding-left:20px;padding-right:20px;"></table>
<table width="600" cellspacing="0" cellpadding="0" border="0" style="border:0;background-color:#ffffff;">
<tbody>
<tr>
<td width="200" valign="middle" bgcolor="#efefef" height="60" align="left" style="padding:10px 0 10px 30px;text-align:right;">
<div style="font-size:14px;letter-spacing:1px;font-weight:bold;color:#787878;">
<a target="_blank" href="{{url("/")}}" style="text-decoration: none">
<h1><strong style="color:#616161;">Arepachan</strong></h1>
</a>
</div>
</td>
</tr>
</tbody>
</table>
<table width="600" cellspacing="0" cellpadding="0" border="0" style="border:1px solid #dddddd;background-color:#ffffff;">
<tbody>
<tr>
<td valign="top" align="left" style="padding:30px 30px 0 30px;font-family:Arial;color:#444444;font-size:14px;line-height:1.5em;" colspan="2">
<div style="font-size:14px;line-height:1.6em;color:#444444;text-align:left;">
<p>Hemos recibido una solicitud de recuperar contraseña de {{$user->email}}.</p>
<p>Presione
<a target="_blank" href="{{url('password/recover/'.$token)}}">
aqui</a>
para reestablecer la contraseña.</p>
 <p>Si el link anterior no funciona, por favor intentelo de nuevo o contactenos.</p>
 <p>Gracias,</p>
 <p>Arepachan, Soporte</p>
 </div>
 </td>
 </tr>
 </tbody>
 </table>
 <table width="600" cellspacing="0" cellpadding="0" border="0" style="background-color:#efefef;border:0;border-bottom:0;">
 <tbody>
<tr><td style="color:#808080;font-size:12px;line-height:150%;padding-top:0;padding-right:0;padding-bottom:0;padding-left:0;text-align:center;">
 <div>Este correo electrónico fue enviado porque {{$user->email}} esta registrado en <a target="_blank" href="{{url("/")}}">Arepachan.com</a>.</div>
 </td>
 </tr>
 <tr>
 <td style="color:#808080;font-size:12px;line-height:150%;padding-top:0;padding-right:0;padding-bottom:20px;padding-left:0;text-align:center;">
 <div>Por favor no responda directamente a este mensaje. Si tiene alguna pregunta dirijase a la sección de contacto en la página.
 </div>
 </td>
 </tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>