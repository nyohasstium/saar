﻿

    <div class="box-body">
    <p class="help-block text-right"><span class="text-danger">*</span> Campos obligatorios</p>
  <div class="form-group">
    <label for="nombre" class="col-sm-2 control-label">Nombre<span class="text-danger">*</span></label>
    <div class="col-sm-10">
     {!! Form::text('nombre', null, [ 'class'=>"form-control", $disabled, "placeholder"=>"Nombre del Módulo", "maxlength"=>"100", "mayuscula"]) !!}

    </div>
  </div>
  <div class="form-group">
    <label for="descripcion" class="col-sm-2 control-label">Descripción</label>
    <div class="col-sm-10">
      {!! Form::text('descripcion', null, [ 'class'=>"form-control", $disabled, "placeholder"=>"Descripción del Módulo", "mayuscula"]) !!}

    </div>
  </div>
  <div class="form-group">
                             <div class='col-xs-6 text-center'>
                              <label><strong>Conceptos sin módulos</strong></label>
                              </div>

                              <div class='col-xs-6 text-center'>
                              <label><strong>Conceptos asignados al módulo</strong></label>
                              </div>
    <div class="col-sm-12">
        <select multiple="multiple" id="conceptos-select" name="conceptos[]" {{$disabled}}>
                    @if(isset($conceptosSinModulo))
            @foreach($conceptosSinModulo as $concepto)
                <option value="{{$concepto->id}}">{{$concepto->nompre}}</option>
            @endforeach
                @endif
            @foreach($modulo->conceptos as $concepto)
                <option value="{{$concepto->id}}" selected>{{$concepto->nompre}}</option>
            @endforeach
        </select>
    </div>
  </div>
    </div><!-- /.box-body -->
    @if($disabled!="disabled")
    <div class="box-footer text-right">
        <button class="btn btn-primary"> {{$SubmitBtnText}} </button>
    </div>
    @endif