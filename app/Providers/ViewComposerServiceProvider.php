<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;


class ViewComposerServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{


        \Html::macro('sortableColumnTitle', function($title, $varName){
            return "<th style='cursor:pointer' class='sortable-table-title' data-sort-name='".$varName."'> $title <span class='".((array_get(\Input::all(),"sortName") == $varName)?((array_get(\Input::all(),"sortType")=='ASC')?'glyphicon glyphicon-sort-by-attributes':'glyphicon glyphicon-sort-by-attributes-alt'):'glyphicon glyphicon-sort')."'></span></th>";
        });

        \Html::macro('pagination', function($page){

           return "Mostrando: ".(($page->currentPage()-1)*$page->perPage()+1)." - ".(($page->currentPage()-1)*$page->perPage()+$page->count())." de ".$page->total();
        });

		view()->composer(['partials.navbar', 'partials.menu'], function($view){
            $createdAt=$userName="";
            $user=\Auth::user();
            if($user){
                $userName=ucwords($user->username);
                $createdAt=$user->created_at;
            }
            $view->with(compact('userName', 'createdAt'));
        });

        view()->composer(['aeronaves.partials.index'], function($view){
            $nacionalidades= \App\NacionalidadMatricula::lists('nombre', 'siglas', 'id');
            $view->with(compact('nacionalidades'));
        });

        view()->composer(['cliente.partials.form', 'aeronaves.index', 'aeronaves.partials.form'], function($view){
            $hangars= \App\Hangar::where("aeropuerto_id","=", $this->getAeropuertoId())->lists('nombre', 'id');
            $view->with(compact('hangars'));
        });

        view()->composer(['index','cliente.partials.form','factura.partials.form'], function($view){
            $aeropuertos= \App\Aeropuerto::lists('nombre', 'id');
            $view->with(compact('aeropuertos'));
        });

        view()->composer(['cliente.partials.form'], function($view){
            $paises = \App\Pais::lists('nombre','id');
            $view->with(compact('paises'));
        });


        view()->composer(['contrato.partials.form','factura.partials.form'], function($view){
            $clientes=\App\Cliente::select('codigo', 'id', 'nombre','cedRif','cedRifPrefix')->get();
            $view->with(compact('clientes'));
        });

        view()->composer(['contrato.partials.form'], function($view){
            $conceptos=[""=>"-- Seleccione un concepto --"]+\App\Concepto::where("aeropuerto_id","=", $this->getAeropuertoId())->orderBy('nompre', 'ASC')->lists('nompre', 'id');
            $view->with(compact('conceptos'));
        });

        view()->composer(['factura.partials.form'], function($view){
            $route=\Route::current();
            $params=$route->parameters();
            $moduloNombre=($params["modulo"]=="Todos")?"%":$params["modulo"];
            $modulo=\App\Modulo::where("nombre", "like", $moduloNombre)->where("aeropuerto_id","=", $this->getAeropuertoId())->lists("id");
            $conceptos=\App\Concepto::select('nompre', 'id', 'iva')->whereIn("modulo_id",$modulo)->orderBy('nompre', 'ASC')->get();
            $view->with(compact('clientes', 'conceptos'));
        });

	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}


    protected function getAeropuertoId(){
        $aeropuertoId=session('aeropuerto');
        if($aeropuertoId)
            $aeropuertoId=$aeropuertoId->id;
        return $aeropuertoId;
    }

}
