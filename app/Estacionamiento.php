<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Estacionamiento extends Model {



    protected $fillable = ['nTurnos', 'nTaquillas'];

    protected $hidden = ['created_at', 'updated_at'];


    public function portons()
    {
        return $this->hasMany('App\Estacionamientoporton');
    }

    public function conceptos()
    {
        return $this->hasMany('App\Estacionamientoconcepto');
    }

    public function clientes()
    {
        return $this->hasMany('App\Estacionamientocliente');
    }
}
