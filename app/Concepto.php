<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Concepto extends Model {

    protected $guarded = array();

    public function modulo()
    {
        return $this->belongsTo('App\Modulo');
    }
    public function aeropuerto()
    {
        return $this->belongsTo('App\Aeropuerto');
    }
    public function facturadetalles()
    {
        return $this->hasMany('App\Facturadetalle');
    }
}
