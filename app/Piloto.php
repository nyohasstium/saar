<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Piloto extends Model {

    //
    public function nacionalidad()
    {
        return $this->belongsTo('App\Pais');
    }

}
